========
Releases
========

2022-jul v1.0 First release (amongst others): 
    - iMSTK Version 6.0
    - Support to import VTK and other mesh formats (e.g. vtk, vtu, stl, ply, veg, …)
    - Deformable & Rigid Body Models to be used in Unity
    - Custom Editors for iMSTK Behaviors
    - Helper Scripts to create simple dynamic objects

