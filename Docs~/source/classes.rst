Usage
=====

For minimal usage of iMSTK-Unity, two things must be added to a Unity Scene.

   - A ``GameObject`` with a ``SimulationManager`` attached to it
   - A ``GameObject`` with a Model attached to it.

Commonly a PhysicsGeometry is also needed on the Model ``GameObject``.


Component Structure
==============================

While the iMSTK C# wrapper supports almost all iMSTK classes. There is a subset that is made available as Unity components. These can be assembled in the editor to create simulations using iMSTK inside of Unity. The following section describes the roles and responsibilities of the available iMSTK-Unity classes.

Please note that while most components can be enabled and disabled in the Editor this will only be effective during the editing process. Disabled components will not be used for simulation. _BUT_ enabling or disabling a component during runtime will not affect the simulation and may cause issues.

Infrastructure
------------------------------

``SimulationManager``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is a component responsible for controlling the simulation. There may only exist one. It also controls the construction, initialization, and destruction of ``iMSTKBehaviour`` to ensure execution ordering: 

    - Simulation Manager created
    - iMSTK objects created and internally initialized
    - iMSTK objects externally initialized
    - ``SimulationManager`` Start
    - Updates 
    - iMSTK objects cleaned up 
    - ``SimulationManager`` cleaned up.

This component is required to be in the scene for simulations to run. It is created before any other iMSTK components on any ``GameObject``. It implements the start, stop, pause, and other global scene related tasks.

``iMSTKBehaviour``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
An extension of MonoBehaviour to provide different callbacks for special construction, initialization, and destruction ordering.

``ConnectiveTissue``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This component represents connective tissue as a multitude of strands between opposing surfaces. Given two opposing geometries strands will be generated with configurable parameters. The generated object is physical and can be interacted with. The connective tissue will consist of multiple "strands" each going from one of the reference objects to the other. Each strand will be made up of a given number of segments. The amount of strands is roughly the number of faces on one bounding object * ``segmentsPerFace``. Note that increasing the density and/or the number of segments per strand will also increase the computational load to simulate this object.

``GeometryFilter``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Similar to a MeshFilter in Unity. It provides an input and output geometry. It may take in any iMSTK geometry, as well as a Unity Mesh (one can also drag/drop a MeshFilter to it). These are instances of geometries used in all of iMSTK unity scripts.

``GeometryMap``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Allows the use of separate meshes for the deformable, visual and collision representation. Will move the vertices of the target mesh according to matching points on the source mesh. The points do not have to completely coincide.

``RigidController``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Object used between a device handled by the user and a ``Rigid``. It utilizes a mass spring system to correct for latency in the system. It corrects for problems with haptics in simulation systems. By manipulating the spring parameters the haptic response can be tuned to the behavior of the computer and the simulated system.

``BoundaryCondition``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use this behavior to mark vertices on a deformable object as `fixed`. This means they won't move but are still part of the overall system. In general this will mean that the object will be attached to the points selected. As the shape assigned can be any mesh this is an easy way to fix an object in space.

Importers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
iMSTK-Unity provides a custom Unity importer to import geometry using iMSTK. This can import point, line, surface, tetrahedral, & hexahedral meshes (vtk, vtu, stl, ply, veg, ...). If the mesh imported is a point, line, or surface mesh then it will be imported as a Unity Mesh object. Anything else not supported by Unity, is loaded as an iMSTK-Unity Geometry Object. When a volumetric mesh (such as a tetrahedral mesh) is imported the accompanying surface is extracted and provided as an additional asset.

Editor Scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
iMSTK-Unity provides extensions to the Unity editor. These extensions include:

   - Custom inspectors for the models and geometry components.
   - A global settings menu.
   - Menu Items for quick creation of GameObject with iMSTK items already setup.
   - Editors/windows for various operations


Models
------------------------------

``Deformable``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use this to represent deformable objects. Position Based Dynamics (PBD), is used to model the deformation. This model supports Lines (1D), Surface Meshes (2D) and, Tetrahedral Meshes (3D) dynamical models see the `iMSTK Documentation <https://imstk.readthedocs.io/en/latest/PbdModel.html>`__ for more information on constraints and models. Visual, physics and collision geometry can be assigned separately. If you do, a separate map will be necessary to update the various meshes. 

The physics geometry determines the type of constraint that can be used, an invalid constraint may cause problems. 

.. list-table:: Valid Constraint Combinations
   :header-rows: 1

   * - Physics Geometry Type
     - Valid Constraints
   * - Line Mesh (Threads)
     - Distance Stiffness, Bend Stiffness
   * - Surface Mesh (Membranes, Bags)
     - Distance Stiffness, Dihedral Stiffness, Area Stiffness
   * - Volumentric Mesh (Tissue)
     - Distance Stiffness, Volume Stiffness, Fem (all models)

``RbdModel``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning:: 
This class has been deprecated, use ``Rigid`` instead. This utilizes the old rigid implementation and all of the functionality regarding to this class will be removed in the next version.

Use this to represent moveable rigid objects like forceps or scalpels. Visual and collision geometry can be assigned separately, this behavior will update the transform of the GameObject with each update. For more information see the `iMSTK Documentation <https://imstk.readthedocs.io/en/latest/Dynamical_Models.html#rigid-body-2>`__ 

``Rigid``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use this to represent moveable rigid object use this to represent moveable rigid objects like forceps or scalpels. Physics and collision geometry can be assigned separately, this behavior will update the transform of the GameObject with each update. The ``Rigid`` class internally is implemented via the PBD Model.

``StaticModel``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use this to represent un-moveable rigid objects like the ground plane or other obstacles.

Interactions
------------------------------

``CollisionInteraction``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use this behavior to set up collisions between two objects, in general this behavior can detect what the type of the two objects is that are interacting (mode `Auto`). But you can also select the algorithm that should be used.

``Grasping``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
s
This class is used to enable grasping between a rigid object and a deformable. Assigning a rigid and a deformable, then use ``StartGrasp()`` and ``EndGrasp()`` to drive the grasping. ``useVertexGrasping`` indicates that the grasped item on the deformable will be a vertex, otherwise it will be a whole cell (e.g. triangle)

When grasped iMSTK will attempt to maintain the relation between the grasped and the grasping geometry. When grasped iMSTK will attempt to maintain the relation between the grasped and the grasping geometry. When the rigid is moved the opposite side will attempt to move with it.

``PbdObjectInteraction``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A collision interaction specific to PBD models, it allows setting of values specific to interactions with PBDs.

``PbdRigidGraspingInteraction``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning:: 
This class has been deprecated, use ``Grasping`` instead. This utilizes the old rigid implementation and all of the functionality regarding to this class will be removed in the next version.

An interaction that allows the grasping of objects, when activated it will attach a point on the rigid to a point on the deformable object and will attempt to keep the two in the same place.


Devices
------------------------------

``OpenHapticsDevice``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This device is only available with a custom build of iMSTK. It enables the use of the `3DSystems <https://www.3dsystems.com/haptics>`_ haptic device family.

``VrpnDevice``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This device is only available with a custom build if iMSTK. It enables interactions with devices run by a `VRPN <https://github.com/vrpn/vrpn>`_ server.

Other
------------------------------

``SimulationStats``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This component can be used to display timing information on the game screen, this is aside the data that is pushed to the profiler. The data shown is the ``Update()`` rate, the avg. time used to run 1 physics update `simulationManager->advance()` and information about mesh updates.






