.. centered:: |image0|

Welcome to iMSTK-Unity
======================

`iMSTK <https://www.imstk.org/>`_ is a free & open source C++ toolkit for the prototyping of interactive multi-modal surgical simulations. The iMSTK-Unity asset gives you access to its capabilities through the Unity authoring interface. While this asset is still under development  you can already exercise a variety of parts of iMSTK inside of Unity. This guide will help you get acquainted with the architecture and workings of the plugin.

The latest version of this documentation can be found on `ReadTheDocs <https://imstk-unity.readthedocs.io/en/latest/>`_

.. toctree:: 
   :maxdepth: 2
   :caption: Introduction

   documentation

.. toctree:: 
   :maxdepth: 1
   :caption: User guide

   guide

.. toctree:: 
   :maxdepth: 2
   :caption: Class List

   classes

.. toctree:: 
   :maxdepth: 0
   :caption: Release Notes
   
   releases
   
.. toctree:: 
   :maxdepth: 1
   :caption: Licensing

   license

Other Resources
----------------------
   - `iMSTK Discourse Forum <https://discourse.kitware.com/c/imstk/9>`_
   - `iMSTK-Unity Gitlab <https://gitlab.kitware.com/iMSTK/imstk-unity/>`_
   - `iMSTK Documentation <https://imstk.readthedocs.io/en/latest/>`_
   - `iMSTK Gitlab <https://gitlab.kitware.com/iMSTK/iMSTK>`_

.. centered:: |image1|

.. |image0| image:: media/imstk-logo.png
   :width: 3.5in
   :height: 1.28515625in

.. |image1| image:: media/kitware-logo.jpg
   :height: 1in