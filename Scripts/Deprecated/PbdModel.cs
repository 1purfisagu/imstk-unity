﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImstkUnity
{
    [System.Obsolete("PbdModel is deprecated, please use the Deformable component instead, " +
       "PbdModel will be removed in a future version of the iMSTK-Unity Asset")]
    class PbdModel : Deformable
    {
    }
}
