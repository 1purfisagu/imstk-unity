﻿using ImstkUnity;
using UnityEditor;
using UnityEngine;

namespace ImstkEditor
{
    [CustomEditor(typeof(ConnectiveTissue))]
    public class ConnectiveTissueEditor : Editor
    {
        // Local variables for caching editor results
        Deformable sideA;
        Deformable sideB;

        double maxDistance;
        double strandsPerFace;
        int segmentsPerStrand;
        double distanceStiffness;

        GUIContent sideAContent = new GUIContent("Side A", "One side of the objects that should be connected.");
        GUIContent sideBContent = new GUIContent("Side B", "One side of the objects that should be connected.");
        GUIContent maxDistContent = new GUIContent("Maximum Distance", "If side a and b are closer than this value" +
            " connective tissue strands will be generated. If 0 the distance between the centers will be used");
        GUIContent strandsPerFaceContent = new GUIContent("Strands per Face", "Indicates the density of strands " +
            "that are being generated, fractions can be used e.g. 0.5 will generate a strand for half the faces");
        GUIContent segmentsPerStrandContent = new GUIContent("Segments per Strand", "Determines the number of " +
            "segments for each strand");
        GUIContent distanceStiffnessContent = new GUIContent("Distance Stiffness", "Determines how much the " +
            "connective tissue will resist extension.");
        public override void OnInspectorGUI()
        {
            var script = target as ConnectiveTissue;

            EditorGUI.BeginChangeCheck();

            sideA = EditorGUILayout.ObjectField(sideAContent, script.objectA, typeof(ImstkUnity.Deformable), true) as ImstkUnity.Deformable;
            sideB = EditorGUILayout.ObjectField(sideBContent,script.objectB, typeof(ImstkUnity.Deformable), true) as ImstkUnity.Deformable;

            maxDistance = EditorGUILayout.DoubleField(maxDistContent, script.maxDistance);
            strandsPerFace = EditorGUILayout.DoubleField(strandsPerFaceContent, script.strandsPerFace);
            segmentsPerStrand = EditorGUILayout.IntField(segmentsPerStrandContent, script.segmentsPerStrand);
            distanceStiffness = EditorGUILayout.DoubleField(distanceStiffnessContent, script.distanceStiffness);

            if (EditorGUI.EndChangeCheck())
            {
                if ((sideA == null && sideB == null) || sideA != sideB)
                {
                    script.objectA = sideA;
                    script.objectB = sideB;
                }

                script.maxDistance = maxDistance;
                script.strandsPerFace = strandsPerFace;
                script.segmentsPerStrand = segmentsPerStrand;
                script.distanceStiffness = distanceStiffness;
            }
        }
    }
}